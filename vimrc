set nocompatible

call pathogen#runtime_append_all_bundles()
call pathogen#helptags()

:syntax enable
set background=dark
:colorscheme solarized
set softtabstop=2
set autoindent
set smartindent
set nowrap
set tabstop=2
set sw=2
filetype indent on
set nobackup
set nowritebackup
set noswapfile
set number
filetype on
filetype plugin on
set guifont=Inconsolata:h12
set wildmenu
set hlsearch
set expandtab

" Turn off that stupid highlight search
nmap <silent> ,n :set invhls<CR>:set hls?<CR>


:nmap <D-d> :NERDTree<CR> 
imap <C-l> <Space>=><Space>


function! MoveLineUp()
  call MoveLineOrVisualUp(".", "")
endfunction

function! MoveLineDown()
  call MoveLineOrVisualDown(".", "")
endfunction

function! MoveVisualUp()
  call MoveLineOrVisualUp("'<", "'<,'>")
  normal gv
endfunction

function! MoveVisualDown()
  call MoveLineOrVisualDown("'>", "'<,'>")
  normal gv
endfunction

function! MoveLineOrVisualUp(line_getter, range)
  let l_num = line(a:line_getter)
  if l_num - v:count1 - 1 < 0
    let move_arg = "0"
  else
    let move_arg = a:line_getter." -".(v:count1 + 1)
  endif
  call MoveLineOrVisualUpOrDown(a:range."move ".move_arg)
endfunction

function! MoveLineOrVisualDown(line_getter, range)
  let l_num = line(a:line_getter)
  if l_num + v:count1 > line("$")
    let move_arg = "$"
  else
    let move_arg = a:line_getter." +".v:count1
  endif
  call MoveLineOrVisualUpOrDown(a:range."move ".move_arg)
endfunction

function! MoveLineOrVisualUpOrDown(move_arg)
  let col_num = virtcol(".")
  execute "silent! ".a:move_arg
  execute "normal! ".col_num."|"
endfunction

nnoremap <silent> <C-Up> :<C-u>call MoveLineUp()<CR>
nnoremap <silent> <C-Down> :<C-u>call MoveLineDown()<CR>
inoremap <silent> <C-Up> <C-o>:<C-u>call MoveLineUp()<CR>
inoremap <silent> <C-Down> <C-o>:<C-u>call MoveLineDown()<CR>
vnoremap <silent> <C-Up> :<C-u>call MoveVisualUp()<CR>
vnoremap <silent> <C-Down> :<C-u>call MoveVisualDown()<CR>

let mapleader=","
 
let g:fuzzy_ignore = "*.log"
let g:fuzzy_ignore = "gems/*"
let g:fuzzy_ignore = ".git"
let g:fuzzy_matching_limit = 60
 
nnoremap <leader>b :FufBuffer<CR>
nmap <leader>fb :FufBuffer<CR>
nmap <leader>ff :FufFile<CR>

:command W w
nmap <leader>ftj :set ft=javascript<CR>
nmap <leader>fth :set ft=html<CR>
nmap <leader>ftc :set ft=css<CR>

let mapleader=','
if exists(":Tabularize")
  nmap <Leader>a= :Tabularize /=<CR>
  vmap <Leader>a= :Tabularize /=<CR>
  nmap <Leader>a: :Tabularize /:\zs<CR>
  vmap <Leader>a: :Tabularize /:\zs<CR>
endif

if has("gui_running")
    set guioptions=egmrt
endif

nnoremap <leader>yr :YRShow<CR>
